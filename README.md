# steakcoin

`steakcoin` is a simple cryptocurrency inspired by [Peercoin](https://www.peercoin.net/) and [Tezos](https://tezos.com/).
The consensus mechanism used is a hybrid containing proof-of-stake and proof-of-burn elements.
`steakcoin` was created as a class project for [CS 295/395: Secure Distributed Computation](https://jnear.github.io/cs295-secure-computation/).
As such, it is a prototype cryptocurrency with limited functionality in comparison to the projects that inspired it.
The main purpose of `steakcoin` is to serve as a learning experience with a focus on cryptocurrency design and Rust language features.

## Contents:
- `proposal/`: Describes the premise of `steakcoin` and rough project direction.
- `simulation/`: A zero-intelligence agent-based model to display the functionality of steakcoin.
- `steakcoin-lib/`: The `steakcoin` implementation.
- `writeup/`: Summarizes the current state of `steakcoin` and some of the design decisions that got us here.

## Getting started:
Before you start, ensure that you have [Rust](https://www.rust-lang.org/tools/install) and [Git](https://git-scm.com/) installed.
- Clone the repository: `git clone git@gitlab.com:cvanoort/steakcoin.git`
- Check out the simulation: `cd steakcoin/simulation && cargo run --release`
- Checkout the implementation in `steakcoin/steakcoin-lib`
