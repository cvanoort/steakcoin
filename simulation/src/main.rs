extern crate steakcoin_lib;

use std::collections::{HashMap, VecDeque};

use chrono::Utc;
use openssl::pkey::{PKey, Private};
use openssl::rsa::Rsa;
use rand::prelude::ThreadRng;
use rand::Rng;
use rand::seq::IteratorRandom;
use rand_distr::{Distribution, LogNormal};

use steakcoin_lib::*;

#[derive(Clone, Eq, PartialEq)]
enum Message {
    Transaction(Transaction),
    Block(Block),
    Signature(Block),
}

struct Client {
    key: PKey<Private>,
    current_stakes: HashMap<Vec<u8>, i64>,
    messages: VecDeque<Message>,
    blockchain: Blockchain,
    log_normal: LogNormal<f64>,
    pending_transactions: VecDeque<Transaction>,
    potential_blocks: Vec<Block>,
    rights_order: Vec<Vec<u8>>,
}

impl Client {
    fn new() -> Client {
        Client {
            key: PKey::from_rsa(Rsa::generate(512).unwrap()).unwrap(),
            current_stakes: HashMap::new(),
            messages: VecDeque::new(),
            blockchain: Blockchain::new(),
            log_normal: LogNormal::new(3.0, 2.0).unwrap(),
            pending_transactions: VecDeque::new(),
            potential_blocks: vec![],
            rights_order: vec![],
        }
    }

    fn get_action_msg(&mut self, timestamp: i64, payee: &Vec<u8>, rng: &mut ThreadRng) -> Message {
        let x: f32 = rng.gen();
        if x > 0.5 {
            self.transaction_msg(MINT_BOND as u64, vec![], timestamp, true)
        }
        // else if x > 0.7 {
        //     self.transaction_msg(ENDORSE_BOND as u64, vec![], timestamp, true)
        // }
        else {
            let amount = self.log_normal.sample(rng) as u64;
            self.transaction_msg(amount, payee.clone(), timestamp, false)
        }
    }

    fn transaction_msg(&mut self, amount: u64, payee: Vec<u8>, timestamp: i64, bond: bool) -> Message {
        let mut transaction = Transaction {
            payer: self.key.public_key_to_der().unwrap(),
            payee,
            amount,
            timestamp,
            valid_hash: None,
            payer_signature: vec![],
            bond,
        };

        transaction.sign(&self.key);

        Message::Transaction(transaction)
    }


    fn mint_msg(&mut self, timestamp: i64) -> Option<Message> {
        // select transactions
        if !self.rights_order.is_empty() && self.rights_order[0] == self.key.public_key_to_der().unwrap() {
            let mut discard = 0;
            let mut stakes = self.current_stakes.clone();
            let mut block = Block::new();
            let mut idx = 0;
            for transaction in self.pending_transactions.iter() {
                if idx == TRANSACTIONS_PER_BLOCK {
                    break;
                }
                let payer_holdings = stakes.get(&transaction.payer).unwrap_or(&0i64);
                if *payer_holdings >= transaction.amount as i64 && transaction.amount > 0 {
                    *stakes.get_mut(&transaction.payer).unwrap() -= transaction.amount as i64;
                    block.transactions[idx] = transaction.clone();
                    idx += 1;
                } else {
                    discard += 1;
                }
            }
            if idx == TRANSACTIONS_PER_BLOCK {
                block.timestamp = timestamp;
                block.block_id = self.blockchain.blocks.len();
                println!("Proposing new block at level {}, discarded {} invalid transactions", block.block_id, discard);
                block.sign(&self.key);
                Some(Message::Block(block))
            } else {
                None
            }
        } else {
            None
        }
    }

    fn endorse_msg(&mut self) -> Message {
        let idx = self.blockchain.blocks.len() - 1;
        self.blockchain.blocks[idx].endorse(&self.key);
        Message::Signature(self.blockchain.blocks[self.blockchain.blocks.len() - 1].clone())
    }

    fn add_block(&mut self, block: Block) {

        for transaction in block.transactions.iter() {

            // remove used transactions and ignored transactions
            let mut buf_transaction = self.pending_transactions.pop_front().unwrap();
            while buf_transaction != *transaction {
                buf_transaction = self.pending_transactions.pop_front().unwrap();
            }
        }

        self.blockchain.add_block(block, None);
        self.current_stakes = self.blockchain.get_holdings();
        self.rights_order = self.blockchain.get_minting_rights_ordering();
    }

    fn process_messages(&mut self) {
        while !self.messages.is_empty() {
            let msg = self.messages.pop_front().unwrap();
            match msg {
                Message::Transaction(transaction) => {
                    // check if valid and non-negative
                    if transaction.validate_sig() {
                        self.pending_transactions.push_back(transaction.to_owned());
                    }
                }
                // todo use chain with most endorsements
                Message::Signature(block) => {
                    if block.validate_endorsements().iter().all(|&x| x == true) {
                        let mut signed = false;
                        if self.blockchain.blocks.len() >= block.block_id {
                            if self.blockchain.blocks[block.block_id] == block {
                                self.blockchain.blocks[block.block_id].merge_endorsements(&block);
                                signed = true;
                            }
                        }

                        let mut idx = 0;
                        while idx < self.potential_blocks.len() && !signed {
                            if self.potential_blocks[idx] == block {
                                self.potential_blocks[idx].merge_endorsements(&block);
                                signed = true;
                            }
                            idx += 1;
                        }
                    }
                }
                Message::Block(block) => {
                    if block.block_id == self.blockchain.blocks.len() {
                        self.add_block(block);
                    } else if block != self.blockchain.blocks[self.blockchain.blocks.len() - 1] {
                        println!("Conflicting block");
                        self.potential_blocks.push(block)
                    }
                }
            }
        }
    }
}

fn init(num_clients: usize, num_blocks: u64) -> Vec<Client> {
    let mut rng = rand::thread_rng();
    let mut clients = vec![];
    for _ in 0..num_clients {
        clients.push(Client::new())
    }

    let mut transactions = vec![Transaction::new(); ((num_blocks - 1) * 64) as usize];
    let log_normal = LogNormal::new(10.0, 3.0).unwrap();

    for transaction in transactions.iter_mut() {
        transaction.payee = clients.iter().choose(&mut rng).unwrap().key.public_key_to_der().unwrap();
        transaction.amount = log_normal.sample(&mut rng) as u64;
        transaction.timestamp = Utc::now().timestamp();
    }

    let mut blockchain = Blockchain::new();
    for i in 0..num_blocks - 1 {
        let mut block = Block::new();
        block.transactions = transactions[i as usize * 64..i as usize * 64 + 64].to_vec();
        for signer in clients.iter().choose_multiple(&mut rng, ENDORSERS_PER_BLOCK) {
            block.sign(&signer.key);
        }
        blockchain.add_block(block, None);
    }

    let mut holdings = blockchain.get_holdings();
    let pub_keys: Vec<Vec<u8>> = holdings.keys().cloned().collect();

    let mut bonds = vec![Transaction::new(); TRANSACTIONS_PER_BLOCK];

    // setup block of bonds
    for (idx, transaction) in bonds.iter_mut().enumerate() {
        let bond_val = if idx < 15 {
            MINT_BOND
        } else {
            ENDORSE_BOND
        };

        let mut payer = pub_keys.iter().choose(&mut rng).unwrap();

        while holdings.get(payer).unwrap() < &bond_val {
            payer = pub_keys.iter().choose(&mut rng).unwrap();
        }

        *holdings.get_mut(payer).unwrap() -= bond_val;
        transaction.payer = payer.clone();
        transaction.amount = bond_val as u64;
        transaction.bond = true;
    }

    let mut block = Block::new();
    block.transactions = bonds;
    for signer in clients.iter().choose_multiple(&mut rng, ENDORSERS_PER_BLOCK) {
        block.sign(&signer.key);
    }
    blockchain.add_block(block, None);

    let holdings = blockchain.get_holdings();
    let rights = blockchain.get_minting_rights_ordering();
    for client in clients.iter_mut() {
        client.blockchain = blockchain.clone();
        client.current_stakes = holdings.clone();
        client.rights_order = rights.clone();
    }

    clients
}

// clients do one minute ordering
fn main() {
    let mut clients = init(100, 3);
    let mut rng = rand::thread_rng();
    let mut timestamp = Utc::now().timestamp();
    let rounds = 1000;
    let pub_keys: Vec<Vec<u8>> = clients.iter().map(|x| x.key.public_key_to_der().unwrap()).collect();

    for r in 0..rounds {
        let mut messages = VecDeque::new();
        for client in clients.iter_mut() {
            timestamp += 1;
            match client.mint_msg(timestamp) {
                None => {}
                Some(msg) => { messages.push_back(msg) }
            }
            let payee = pub_keys.iter().choose(&mut rng).unwrap();
            messages.push_back(client.get_action_msg(timestamp, payee, &mut rng))
        }

        for client in clients.iter_mut() {
            client.messages.append(&mut messages.clone());
            client.process_messages();
        }

        let total: Vec<i64> = clients[0].current_stakes.values().cloned().collect();
        let total: i64 = total.iter().sum();
        println!("\n\nEnd of round {}", r);
        println!("Total Float {}", total);
        println!("There are {} eligible minters", clients[0].rights_order.len());
    }
}
