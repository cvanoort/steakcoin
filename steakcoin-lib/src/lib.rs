use std::collections::{HashMap, VecDeque};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use chrono::Utc;
use openssl::hash::MessageDigest;
use openssl::pkey::{PKey, Private};
use openssl::sha::Sha256;
use openssl::sign::{Signer, Verifier};
use rand::prelude::*;

pub const TRANSACTIONS_PER_BLOCK: usize = 64;
pub const ENDORSERS_PER_BLOCK: usize = 32;
pub const BLOCKS_PER_CYCLE: usize = 16;

pub const ENDORSE_BOND: i64 = 64_00;
pub const ENDORSE_REWARD: i64 = 32_00;
pub const MINT_BOND: i64 = 2048_00;
pub const MINT_REWARD: i64 = 512_00;

pub const BOND_LIFETIME: usize = 3 * BLOCKS_PER_CYCLE;


#[derive(Hash, Clone, Debug, PartialEq, Eq)]
pub struct Transaction {
    // Payer public key
    pub payer: Vec<u8>,
    // Payee public key
    pub payee: Vec<u8>,
    // Amount of steakcoin transacted with minimum increment of cents
    pub amount: u64,
    // When the transaction occurred (Unix time)
    pub timestamp: i64,
    // Hash of a recent block that the client considers valid
    pub valid_hash: Option<u64>,
    // Digital signature of (payee, amount, timestamp, valid_hash) by payer
    pub payer_signature: Vec<u8>,
    // Indicates that this transaction represents a minting or endorsing bond
    pub bond: bool,
}

impl Transaction {
    pub fn new() -> Transaction {
        Transaction {
            payer: Vec::new(),
            payee: Vec::new(),
            amount: 0,
            timestamp: Utc::now().timestamp(),
            valid_hash: None,
            payer_signature: Vec::new(),
            bond: false,
        }
    }

    pub fn sign(&mut self, key: &PKey<Private>) {
        let mut signer = Signer::new(MessageDigest::sha256(), &key).unwrap();
        signer.update(&self.payee).unwrap();
        signer.update(&self.amount.to_be_bytes()).unwrap();
        signer.update(&self.timestamp.to_be_bytes()).unwrap();
        match self.valid_hash {
            Some(hash) => signer.update(&hash.to_be_bytes()).unwrap(),
            None => (),
        }
        self.payer_signature = signer.sign_to_vec().unwrap();
    }

    pub fn validate_sig(&self) -> bool {
        let key = PKey::public_key_from_der(&self.payer).unwrap();

        let mut verifier = Verifier::new(MessageDigest::sha256(), &key).unwrap();
        verifier.update(&self.payee).unwrap();
        verifier.update(&self.amount.to_be_bytes()).unwrap();
        verifier.update(&self.timestamp.to_be_bytes()).unwrap();
        match self.valid_hash {
            Some(hash) => verifier.update(&hash.to_be_bytes()).unwrap(),
            None => (),
        }
        verifier.verify(&self.payer_signature).unwrap()
    }
}


#[derive(Hash, Clone, Debug, PartialEq, Eq)]
pub struct Denunciation {
    // Denunciations call out a minter for double minting or double endorsing.

    // Public key of the offending individual
    pub signer_id: Vec<u8>,
    // Block level where the offense occurred
    pub block_id: usize,
    // The offending signatures
    pub signature_1: Vec<u8>,
    pub signature_2: Vec<u8>,
    // The bonds associated with the offending signatures
    pub bond_1: (usize, usize),
    pub bond_2: (usize, usize),
}

impl Denunciation {
    pub fn validate(&self) -> bool {
        // Prove poor behavior by checking if the signatures decode to the same block_id
        let key = PKey::public_key_from_der(&self.signer_id).unwrap();

        let mut v1 = Verifier::new(MessageDigest::sha256(), &key).unwrap();
        v1.update(&self.block_id.to_be_bytes()).unwrap();

        let mut v2 = Verifier::new(MessageDigest::sha256(), &key).unwrap();
        v2.update(&self.block_id.to_be_bytes()).unwrap();

        v1.verify(&self.signature_1).unwrap() && v2.verify(&self.signature_2).unwrap()
    }
}


#[derive(Hash, Clone, Debug)]
pub struct Block {
    // Height of the current block
    pub block_id: usize,
    // Hash of the blockchain prior to adding this block
    pub pred_hash: Option<u64>,
    // When the block was minted (Unix time)
    pub timestamp: i64,
    // Public key of the minter
    pub minter_id: Vec<u8>,
    // Minter signature of block_id
    pub minter_signature: Vec<u8>,
    // Pointer to a bond transaction, (block_id, transaction_offset)
    pub minter_bond: (usize, usize),
    // Several transactions form a block
    pub transactions: Vec<Transaction>,
    // Index of the next free signer slot
    next_endorser: usize,
    // Public key of signers
    pub endorsers: Vec<Vec<u8>>,
    // Signatures of minters who support the block
    pub endorsements: Vec<Vec<u8>>,
    // Pointers to bond transactions for signers, (block_id, transaction_offset)
    pub endorser_bonds: Vec<(usize, usize)>,
    // Call out bad behavior
    pub denunciation: Option<Denunciation>,
}

impl PartialEq for Block {
    fn eq(&self, other: &Self) -> bool {
        self.block_id == other.block_id &&
            self.pred_hash == other.pred_hash &&
            self.transactions == other.transactions &&
            self.denunciation == other.denunciation
    }
}

impl Eq for Block {}

impl Block {
    pub fn new() -> Block {
        Block {
            block_id: 0,
            pred_hash: None,
            timestamp: Utc::now().timestamp(),
            minter_id: Vec::new(),
            minter_signature: Vec::new(),
            minter_bond: (0, 0),
            transactions: vec![Transaction::new(); TRANSACTIONS_PER_BLOCK],
            next_endorser: 0,
            endorsers: vec![Vec::new(); ENDORSERS_PER_BLOCK],
            endorsements: vec![Vec::new(); ENDORSERS_PER_BLOCK],
            endorser_bonds: vec![(0, 0); ENDORSERS_PER_BLOCK],
            denunciation: None,
        }
    }

    pub fn sign(&mut self, key: &PKey<Private>) {
        let mut signer = Signer::new(MessageDigest::sha256(), &key).unwrap();
        signer.update(&self.block_id.to_be_bytes()).unwrap();
        let signature = signer.sign_to_vec().unwrap();

        self.minter_id = key.public_key_to_der().unwrap();
        self.minter_signature = signature;
    }

    pub fn validate_signature(&self) -> bool {
        let key = PKey::public_key_from_der(&self.minter_id).unwrap();

        let mut verifier = Verifier::new(MessageDigest::sha256(), &key).unwrap();
        verifier.update(&self.block_id.to_be_bytes()).unwrap();
        verifier.verify(&self.minter_signature).unwrap()
    }

    pub fn endorse(&mut self, key: &PKey<Private>) -> bool {
        if self.next_endorser >= ENDORSERS_PER_BLOCK {
            return false;
        }

        let mut signer = Signer::new(MessageDigest::sha256(), &key).unwrap();
        signer.update(&self.block_id.to_be_bytes()).unwrap();
        let signature = signer.sign_to_vec().unwrap();

        self.endorsers[self.next_endorser] = key.public_key_to_der().unwrap();
        self.endorsements[self.next_endorser] = signature;

        self.next_endorser += 1;

        true
    }

    pub fn validate_endorsements(&self) -> Vec<bool> {
        // TODO: return Vec<Option<bool>> and fill unsigned slots with None
        let mut results = vec![false; self.endorsements.len()];
        for (i, (key, signature)) in self.endorsers.iter().zip(self.endorsements.iter()).enumerate() {
            if key.is_empty() & signature.is_empty() { break; }

            let key = PKey::public_key_from_der(key).unwrap();

            let mut verifier = Verifier::new(MessageDigest::sha256(), &key).unwrap();
            verifier.update(&self.block_id.to_be_bytes()).unwrap();
            results[i] = verifier.verify(signature).unwrap();
        }
        results
    }

    pub fn merge_endorsements(&mut self, other: &Self) {
        for i in 0..other.next_endorser {
            let endorser = &other.endorsers[i];
            if (self.next_endorser < ENDORSERS_PER_BLOCK) && !self.endorsers.contains(endorser) {
                self.endorsers[self.next_endorser] = endorser.clone();
                self.endorsements[self.next_endorser] = other.endorsements[i].clone();
                self.endorser_bonds[self.next_endorser] = other.endorser_bonds[i].clone();
                self.next_endorser += 1
            }
        }
    }
}


#[derive(Hash, Clone, Debug)]
pub struct Blockchain {
    pub blocks: Vec<Block>,
    bond_window_start: usize,
}

impl PartialEq for Blockchain {
    fn eq(&self, other: &Self) -> bool {
        if self.blocks.len() == other.blocks.len() {
            for (b1, b2) in self.blocks.iter().zip(other.blocks.iter()) {
                if b1 != b2 {
                    return false;
                }
            }
            return true;
        }
        false
    }
}

impl Eq for Blockchain {}

impl Blockchain {
    pub fn new() -> Blockchain {
        Blockchain {
            blocks: Vec::new(),
            bond_window_start: 0,
        }
    }

    pub fn get_hash(&self) -> u64 {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        s.finish()
    }

    pub fn add_block(&mut self, mut new_block: Block, key: Option<&PKey<Private>>) {
        new_block.block_id = self.blocks.len();
        match key {
            Some(k) => new_block.sign(k),
            None => (),
        };

        match self.blocks.len() {
            0 => new_block.pred_hash = None,
            _ => new_block.pred_hash = Some(self.get_hash()),
        };
        self.blocks.push(new_block);

        if self.blocks.len() > BOND_LIFETIME { self.bond_window_start += 1; }
    }

    pub fn get_holdings(&self) -> HashMap<Vec<u8>, i64> {
        // Calculates the current holdings of the blockchain participants
        // Allows for an arbitrary number of genesis transactions at the start of the blockchain
        // Once the first real transaction occurs, all further genesis transactions are invalid.
        // A block may not mix genesis transactions and non-genesis transactions, so the final genesis block may not be full.
        // TODO: Return an error that can be handled instead of panicking

        let mut holdings: HashMap<Vec<u8>, i64> = HashMap::new();
        let mut id2bond_metas: HashMap<Vec<u8>, VecDeque<(usize, i64)>> = HashMap::new();

        // Handle genesis blocks
        // Genesis blocks are composed of incomplete transactions that create and distribute new coins
        // Along with initial bond submissions to kickstart the minting process
        let mut genesis = true;
        let mut genesis_block_count = 0;
        while genesis && (genesis_block_count < self.blocks.len()) {
            let block = &self.blocks[genesis_block_count];
            for transaction in &block.transactions {
                let amount = transaction.amount as i64;
                if transaction.payer.is_empty() {
                    *holdings.entry(transaction.payee.clone()).or_insert(0i64) += amount;
                } else if transaction.payee.is_empty() & transaction.bond {
                    *holdings.entry(transaction.payer.clone()).or_insert(0i64) -= amount;

                    id2bond_metas
                        .entry(transaction.payer.clone())
                        .or_insert(VecDeque::new())
                        .push_back((block.block_id + BOND_LIFETIME, amount));
                } else {
                    genesis = false;
                    break;
                }
            }
            genesis_block_count += 1;
        }

        // Handle non-genesis blocks
        for block in self.blocks[genesis_block_count..].iter() {
            // Denunciation penalties
            // TODO: The offender should lose their rewards too. May not be worth tracking...
            match &block.denunciation {
                Some(d) => {
                    if d.validate() {
                        // Don't double penalize if they used the same bond
                        if d.bond_1 == d.bond_2 {
                            let (block_1, offset_1) = d.bond_1;
                            let penalty_1 = &self.blocks[block_1].transactions[offset_1].amount;
                            *holdings.entry(d.signer_id.clone()).or_insert(0i64) -= *penalty_1 as i64;
                        }
                        let (block_2, offset_2) = d.bond_2;
                        let penalty_2 = &self.blocks[block_2].transactions[offset_2].amount;
                        *holdings.entry(d.signer_id.clone()).or_insert(0i64) -= *penalty_2 as i64;
                    }
                }
                None => (),
            }

            // Minting rewards
            let metas = id2bond_metas.get(&block.minter_id);
            match metas {
                None => {
                    println!("{:?}", &block.minter_id);
                    panic!("Minter did not have an active bond!");
                }
                Some(_) => {
                    // TODO: Ensure that the minter has a valid bond (amount, no double dip)
                    *holdings.entry(block.minter_id.clone()).or_insert(0i64) += MINT_REWARD;
                }
            };

            // TODO: Endorsement rewards

            // Return expired bonds
            for (id, metas) in &id2bond_metas {
                for (termination, amount) in metas {
                    if &block.block_id > termination {
                        *holdings.entry(id.clone()).or_insert(0i64) += amount;
                    }
                }
            }

            // Apply transactions
            for (j, transaction) in block.transactions.iter().enumerate() {
                let amount = transaction.amount as i64;

                *holdings.entry(transaction.payer.clone()).or_insert(0i64) -= amount;
                if transaction.bond {
                    id2bond_metas
                        .entry(transaction.payer.clone())
                        .or_insert(VecDeque::new())
                        .push_back((block.block_id + BOND_LIFETIME, amount));
                } else {
                    *holdings.entry(transaction.payee.clone()).or_insert(0i64) += amount;
                }

                if *holdings.get(&transaction.payer).unwrap_or(&0i64) < 0i64 {
                    panic!("Invalid transaction (Block {}, T# {}), wallet does not have enough coins ({:?}).", block.block_id, j, transaction.payer)
                }
            }
        }
        holdings
    }

    pub fn get_signer_reward(&self, block_id: usize) -> i64 {
        match block_id {
            0 => { 0 }
            _ => {
                let start = self.blocks[block_id - 1].timestamp;

                // TODO: Should be based off the signing time, not the block creation time
                let end = self.blocks[block_id].timestamp;

                // Difference between timestamps in minutes
                let delta_t = 60 / (end - start);

                ENDORSE_REWARD * delta_t
            }
        }
    }

    pub fn get_minter_candidates(&self) -> Vec<Vec<u8>> {
        let mut bond2termination = HashMap::new();
        for block in &self.blocks[self.bond_window_start..] {
            // TODO: Return an error if a candidate was not eligible to mint
            // TODO: Need to handle multiple active bonds per agent

            // Minter consumed the rights associated with their bond
            bond2termination.remove(&block.minter_id);

            // Track newly deposited bonds
            for transaction in &block.transactions {
                match transaction.bond & (transaction.amount as i64 >= MINT_BOND) {
                    true => bond2termination.insert(transaction.payer.clone(), block.block_id + BLOCKS_PER_CYCLE),
                    false => None,
                };
            }
        }
        let mut candidates: Vec<Vec<u8>> = bond2termination.iter().map(|(k, _)| k.clone()).collect();
        candidates.sort();
        candidates
    }

    pub fn get_minting_rights_ordering(&self) -> Vec<Vec<u8>> {
        let minter_candidates = self.get_minter_candidates();
        let minter_count = minter_candidates.len();

        // Seed a PRNG with the minter ids and block_id
        let mut hasher = Sha256::new();
        for minter in &minter_candidates {
            hasher.update(minter);
        }
        let latest_block_id = self.blocks[self.blocks.len() - 1].block_id;
        hasher.update(&latest_block_id.to_be_bytes());
        let mut rng = StdRng::from_seed(hasher.finish());

        // Create a stake-weighted ordering of minters
        let mut holdings = self.get_holdings();
        holdings.retain(|k, _| minter_candidates.clone().contains(k));
        let mut weighting: Vec<(Vec<u8>, i64)> = holdings.iter().map(|(k, v)| (k.clone(), *v)).collect();
        weighting.sort();

        let mut minter_ordering = Vec::new();
        for _ in 0..minter_count {
            let selected_minter = weighting.choose_weighted(&mut rng, |item| item.1).unwrap().0.clone();

            // Drop the selected minter from holdings (selection without replacement)
            weighting.retain(|item| item.0 != selected_minter);

            minter_ordering.push(selected_minter);
        }
        minter_ordering
    }

    pub fn get_endorser_candidates(&self) -> Vec<Vec<u8>> {
        let mut bond2termination: HashMap<Vec<u8>, VecDeque<usize>> = HashMap::new();
        for block in &self.blocks[self.bond_window_start..] {
            // TODO: Return an error if a candidate was not able to mint
            // TODO: May need to track multiple bonds per agent

            // Endorser consumed the rights associated with their bond
            for i in 0..block.next_endorser {
                match bond2termination.get_mut(&block.endorsers[i]) {
                    Some(metas) => {
                        match metas.is_empty() {
                            true => panic!("Endorser did not have an active bond!"),
                            false => metas.pop_front(),
                        }
                    }
                    None => panic!("Endorser did not have an active bond!")
                };
            }

            // Track newly deposited bonds
            for transaction in &block.transactions {
                match transaction.bond & (transaction.amount as i64 >= ENDORSE_BOND) & (transaction.amount as i64 <= MINT_BOND) {
                    true => bond2termination.entry(transaction.payer.clone()).or_insert(VecDeque::new()).push_back(block.block_id + BLOCKS_PER_CYCLE),
                    false => (),
                };
            }
        }
        bond2termination.iter().map(|(k, _)| k.clone()).collect()
    }
}


#[cfg(test)]
mod tests {
    use openssl::rsa::Rsa;
    use rand::distributions::Uniform;

    use super::*;

    #[test]
    fn integration() {
        let mut rng = rand::thread_rng();
        let amount_dist = Uniform::from(10..10000);
        let time_step_dist = Uniform::from(10..100);

        let wallets: Vec<PKey<Private>> = vec![PKey::from_rsa(Rsa::generate(512).unwrap()).unwrap(); 32];
        let mut transactions = vec![Transaction::new(); 64];
        let mut now = Utc::now().timestamp();
        for transaction in transactions.iter_mut() {
            let ws = wallets.iter().choose_multiple(&mut rng, 2);
            let w_a = ws[0];
            let w_b = ws[1];
            transaction.payer = w_a.public_key_to_der().unwrap();
            transaction.payee = w_b.public_key_to_der().unwrap();
            transaction.amount = amount_dist.sample(&mut rng);
            transaction.timestamp = now;
            transaction.sign(&w_a);

            now += time_step_dist.sample(&mut rng);
        }

        let mut block = Block::new();
        for wallet in &wallets {
            block.endorse(&wallet);
        }
        block.transactions = transactions;

        let mut chain = Blockchain::new();
        chain.add_block(block, None);

        println!("{:#?}", chain)
    }

    #[test]
    fn mining_rights_stability() {
        let mut rng = rand::thread_rng();
        let amount_dist = Uniform::from(2048_00..10000_00);

        let mut wallets: Vec<PKey<Private>> = vec![];
        for _ in 0..TRANSACTIONS_PER_BLOCK {
            wallets.push(PKey::from_rsa(Rsa::generate(512).unwrap()).unwrap());
        }

        let mut chain = Blockchain::new();

        // Genesis block distributing coins
        let mut transactions = vec![Transaction::new(); TRANSACTIONS_PER_BLOCK];
        for i in 0..TRANSACTIONS_PER_BLOCK {
            let wallet = &wallets[i];
            let transaction = &mut transactions[i];
            transaction.payee = wallet.public_key_to_der().unwrap();
            transaction.amount = amount_dist.sample(&mut rng) + MINT_BOND as u64;
        }

        let mut block = Block::new();
        block.transactions = transactions;
        chain.add_block(block, None);

        // Genesis block to seed minting rights
        for _ in 0..2 {
            let mut transactions = vec![Transaction::new(); TRANSACTIONS_PER_BLOCK];
            for i in 0..TRANSACTIONS_PER_BLOCK {
                let wallet = &wallets[i];
                let transaction = &mut transactions[i];
                transaction.payer = wallet.public_key_to_der().unwrap();
                transaction.amount = MINT_BOND as u64;
                transaction.bond = true;
            }

            let mut block = Block::new();
            block.transactions = transactions;
            chain.add_block(block, None);
        }
        let candidates = chain.get_minter_candidates();
        assert_eq!(candidates.is_empty(), false);
        assert_eq!(candidates, chain.get_minter_candidates());

        let minting_rights = chain.get_minting_rights_ordering();
        assert_eq!(minting_rights.is_empty(), false);
        assert_eq!(minting_rights, chain.get_minting_rights_ordering());
    }
}

