\documentclass[11pt,twocolumn]{article}

\usepackage[margin=1in]{geometry}
\usepackage{hyperref}

\title{Steakcoin:\\A Lean Cryptocurrency Without Work}
\author{
John Ring
\and
Colin Van Oort
}
\date{}

\begin{document}
    \maketitle

    \begin{abstract}
        We introduce Steakcoin, a simple cryptocurrency powered by a blockchain with hybrid Proof-of-Stake (PoS) / Proof-of-burn (PoB) consensus.
        We discuss the current, bare bones implementation, the design decisions that lead to this configuration, and potential avenues for future work.
        Our reference implementation is available via GitLab~\cite{steakcoin}.
    \end{abstract}

    \section*{Introduction}\label{sec:intro}
    Cryptocurrencies have flourished since the inception of the Bitcoin blockchain in 2009~\cite{bitcoin-history}.
    Riding a surge of research, development, and public interest, there are now almost 8000 actively traded cryptocurrencies~\cite{crypto-stats}.

    The majority of cryptocurrencies are built on top of a blockchain, which is a ledger that documents all transactions.
    Blockchains are constructed using cryptographic primitives to ensure their integrity over time.
    To avoid the need for a centralized blockchain authority, they are often distributed and maintained by a peer to peer (P2P) network.
    Since participants can join or leave the P2P network at will, and others may be outright malicious, discrepancies can arise in the form of chain forks.
    Forks occur when there are multiple candidate blocks at the same position on the chain.

    A consensus algorithm helps participants decide which candidate block should be considered canonical.
    There are currently at least three distinct solutions to this problem: proof-of-work (PoW)~\cite{nakamoto2019bitcoin}, proof-of-stake (PoS)~\cite{king2012ppcoin}, and proof-of-burn (PoB)~\cite{proofofburn}.
    The PoW system, pioneered by Bitcoin, the creation of a new block requires a participant to solve a hashing puzzle.
    The longest fork, which corresponds to the fork with the greatest PoW, is then accepted as the canonical chain.
    This system has several drawbacks, including susceptibility to a 51\% attack and high energy usage due to the difficulty of the hashing puzzle.
    PoB and PoS aim to reduce the energy spent on maintaining the blockchain by replacing the hashing puzzle with cleverly designed incentive structures.

    PoS allocates mining rights to participants based on the amount of coins they hold.
    Participants with larger amounts of coins suffer greater impacts from damage to the system (via improper or malicious behavior), so they have an incentive to maintain the integrity and responsiveness of the blockchain.
    Since block creation is not inhibited by a difficult hash puzzle, PoS needs to avoid race conditions during block creation.
    One possibly solution is assign an ordering to the miner candidates and restricting when particular miners are allowed to create blocks.
    Additionally, PoS systems need to carefully deal with new forks.
    The nothing-at-stake problem occurs when the cost of fork creation is too low, leading to the creation of many forks and the destruction of consensus.

    In a PoB system, participants must burn coins by sending them to an address where they can no longer be spent.
    This burning process does not consume many resources and provides the participant with mining rights in proportion to the amount burnt.
    These mining rights then allow the participant to create, verify, and add new blocks to the chain.

    PoS was first seen in Peercoin~\cite{peercoin}, which was built on top of the Bitcoin reference implementation approximately 18 months after the start of the Bitcoin blockchain~\cite{peercoin-impl}.
    Coin burning was encountered as a feature of Bitcoin, but was not officially incorporated into a PoB mechanism until 2014 (with the creation of Slimcoin)~\cite{slimcoin}.
    Steakcoin is inspired by Peercoin~\cite{peercoin} and Tezos~\cite{tezos}, adopting a hybrid system that contains elements of PoB and PoS\@.

    \section*{Design of Steakcoin}\label{sec:steakcoin}

    \paragraph{Transactions} represent coin transfers, such as payment from one agent to another or the reservation of coins for a bond.
    An agent initiates a transaction by creating a message containing a payer ID (its own), a payee ID, and an amount of coin to be transferred.
    The message is signed with the agent's private key and disseminated to other agents on SteakNet.

    \paragraph{Bonds} are a special transaction where an agent reserves some coins as collateral in order to gain minting or endorsing rights.
    Minters that create a fork (two candidate blocks at the same chain height) lose their bond, and likewise for endorsers that sign two blocks at the same height.
    If the agent does not display poor behavior during the lifetime of the bond, it it returned to them in full.
    Other minters can identify poor behavior through a denunciation that is attached to a minted block.
    Denunciations take the form of an agent identifier, a block height, and two offending signatures.
    A denunciation can be trivially verified by ensuring that the two signatures can both be decrypted with the given public key resulting in the indicated block height.
    A minter who creates a valid denunciation receives half of the offenders bond, while the other half is burned.
    This incentive structure mitigates nothing-at-stake problem by providing explicit rewards to verifiers and explicit penalties for bad behavior.

    \paragraph{Transaction Validation} is a two part process that any agent with a copy of the blockchain can perform.
    First, the signature of the transaction is verified using the public key of the payer.
    This confirms that the payer authorized the transaction.
    Additionally, the payer must have at a non-negative quantity of coins in their wallet after deducting the transaction amount.

    \paragraph{Blocks} are the foundational structure of the blockchain, storing transactions along with a set of endorsements and other meta-data.
    A newly created block must be signed by the minter before it is proposed to SteakNet.
    This ensures the identity of the minter and is used to implement the denunciation system.
    Each block minted results in a moderate reward of new coins for the minter.
    In our reference implementation each block contains exactly 64 transactions and can be endorsed by at most 32 agents.

    \paragraph{Endorsements} document an agents acceptance of a block.
    These play a crucial role in determining the next canonical block (the one with the most endorsements);
    a realistic concern as multiple valid blocks at the same height could be proposed.
    Endorsers are receive a small reward if the block they signed is added to the chain.
    An endorser who signs an multiple blocks at the same level (double signing) opens themselves to denunciation.
    As there are a finite number of endorsement slots there is a incentive to act quickly, rather than carefully.
    To combat this, the endorsement reward is proportional to the amount of time between the creation of the previous block and the signature time.
    This removes the endorsement reward if the signatures are applied too early, forcing endorsers to consider other candidate blocks if they exist.

    \paragraph{Minting} is the process for adding new blocks to the chain.
    As with endorsing, an agent must submit a bond to gain minting rights.
    Now, allowing any agent who paid an appropriate bond to immediately mint would open the door for large numbers of forks.
    To avoid a PoW puzzle and the fork issue, we instead gate minting rights based on a schedule that is computed deterministically from blockchain information.
    The schedule is constructed by seeding a pseudo random number generator (PRNG) the public keys of all eligible minters, along with the current length of the blockchain.
    Once the PRNG has been seeded, we can now create a random ordering of the eligible minters that is weighted by their respective stakes.
    This ordering defines the time gating schedule for minters of the next block.
    The characteristic length of this schedule is 60 seconds, so once a block is created the first minter in the ordering has 60 seconds to mint a new block before the second minter is given a chance.
    This process reduces the likelihood of forks while promoting good minter behavior.
    As with endorsements, proposing multiple candidate blocks at the same chain height (double minting) opens the agent to a denunciation.

    \section*{Simulation}\label{sec:sim}
    To showcase the features of Steakcoin, we implemented an agent based model of SteakNet.
    Our model features agents: transferring funds, placing bonds, verifying transactions, verifying blocks, and minting new blocks.
    Most agent interactions are zero-intelligence, such as transferring a random number of coins to a random address.
    As a result agents do attempt to perform invalid actions and the health of the system relies on other agents to avoid adding such actions to the blockchain.

    We seed the simulation with several genesis blocks containing transactions that randomly distribute new coins and deposit bonds.
    The coin distribution and bonds are both necessary to start Steakcoin.
    Without the coin distribution no valid transactions can be created, and without bonds there would be no candidate minters to create the first true block.

    Agents in the simulation send messages to one another containing transactions, endorsements and proposed block additions.
    Each agent maintains a copy of the blockchain and updates it based on incoming messages.
    Agents validate the incoming messages and only updates the local blockchain copy when a message is valid.
    Validation is primarily done by minting agents, since they have the strongest incentive to do so.
    Prior to proposing a block, the minter checks their eligibility to mint, validates transaction signatures, and verifies payer assets.

    Although simple, this model exhibits rich behavior.
    The float (amount of spendable coins) trends upwards as the simulation progresses due to the rewards from minting and endorsing.
    However, the float occasionally decreases due to bond placements, which temporarily remove coins from circulation.
    Additionally, the model can enter a locked state without allowing any invalid transactions.
    A locked state occurs when there are no eligible minters due to lack of existing bonds.
    In practice this is unlikely to occur as minters would simply prioritize bond transactions for block inclusion.

    \section*{Conclusion}\label{sec:conclusion}
    We implement Steakcoin, a hybrid PoS/PoB crypto currency and demonstrated its functionality using a simulation.
    Steakcoin provides a simple reference design with pedagogical and prototyping applications.
    This design includes the core aspects of PoS cryptocurrencies such as bond issuance, minting, and endorsement.

    Steakcoin and other cryptocurrencies are ideal candidates for modeling.
    Simple interactions lead to rich behavior and more complex models utilize the competing and tunable incentives inherent in the design.
    Our simulation leverages the full feature set of Steakcoin, allowing us to observe realistic blockchain behavior.

    Future work should improve the efficiency of Steakcoin, particularly incremental blockchain state updates and blockchain validation.
    The simulation is also ripe for future experimentation, with room to investigate more complex agent behaviors.
    Finally, Steakcoin needs a user facing client application to provide an interface to SteakNet and promote use.

    \clearpage

    \bibliographystyle{unsrt}
    \bibliography{writeup}

\end{document}