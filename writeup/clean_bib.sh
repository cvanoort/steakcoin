#!/usr/bin/env bash

biber --tool --output_align --output_indent=4 --output_fieldcase=lower --configfile=biber.conf --validate-config -O writeup.bib writeup.bib
